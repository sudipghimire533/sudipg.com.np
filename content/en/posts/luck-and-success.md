---
title: "Success - bringing luck & hardwork together"
keywords: ["hardwork", "sudip", "luck"]
date: 2022-05-31T09:10:00+05:45
tags: ["philosophy", "lifestyle", "success"]
categories: ["lifestyle"]
description: "Is success solely based on ones hardwork and talent? What if not? What if we are underestimating the role of luck?"
series: []
slug: "success-luck-hardowork"
aliases: []
images: []
toc: true
comment: true
math: false

---
![Blessed leader](https://sudipg.com.np/images/featured-images/luck-and-success.png)

During the Covid lockdown this was quite trending headline: "Nearly half of man say they are doing the most of home schooling and only 3% women agree".
I am bringing this not to debate who is right but I think it's the best example of something call ego-centric bias i.e most people think they did the most of work.

For example, once asking authors of multi-author book about how many percentage of work they have done, sum of those percentage was about 140%. When couple are asked how much of housework they do, the sum also happens to be more than 100% always. One might think it is because people want to think more helpful and generous than they actually are but thats not the case. But that's not it. When couple are asked what percentage of fight they start, again the sum is more than 100%. Looks like we think we did more of the work and also we cause more of the problem. 

Why is this? For me, I think it's because we remember vividly everything we do and  not all of everything what others do. So naturally, you overestimate own contribution and underestimate others. I think, and this bias also lead to influence of other things in our life. For example, the role luck plays in our success.

If you go and ask a hockey player about the reasons for their success, they might mention their hardwork, determination, their family support to wake him at 5.am. But it's unlikely that they will mention how lucky they are to born on January. And yet, in many years 40% of hockey players selected in top teir leauges are born on first quarter of a year compared to just 10% in fourth player. That being said, being born on january increase your luck by 4x to become a hockey player. And the reason for this might preasumebly be cut off dates for kids hockey leauges. Those who are born on january are little bigger and faster than those born on December.  As they grow up, this difference must have shrink to nothing, but it does not. Because kids of first quarter get to spend more time on ice & hockey and also receive better coaching and these advantages compounds year after year. And by the time we get the pros birthdays are heavey skewed towards year. But are there any hockey players who are thankful for their birthday. Probably not. And we all are like this.

Largely obvlious to the fortunate events that support our success. You might feel offended if I point out how bigger role chance plays in their success. And I get it. If we are just the product of circumstances then our hardwork & talent seems to count for nothing. People think it's either skills or luck that explains success. For me, I need both.

For example take 8 track and field world records: all the athletes who achieve these records are obviously world class, extremely dedicated and talented and yet, when they achieved their world records, seven out of right had a tailwind. Noe these athletes all had the ability to win a gold medal, but to set the world record required a bit of luck as well. In same manner, even with the luck accounting for just 5% of the outcome, 9 or 10 of the 11 applicants selected for NASA astronauts programs whould have been completely different if luck played no role at all.

The importance of luck increases the greater the number of applicants applying for just a few space. For example finding only one life partner out of thousands or million of people you see and billions of those who exists. When competition is fierce, being talanted and hard-working is important, but it's not enough to gurantee success. You also need to catch a break.

Largely, I think we are unaware of our good luck because by definition it's not something we did. Like the housework done by your significant others, it goes unappreciated. And here's the interesting thing: downplaying the importance of chance events may actually improve your probability of success because if you perceive an outcome to be uncertain, you are less likely to invest effort in it, which further decreases your chances of success. So it's a useful delusion to believe you are in full control of your destiny.

Now there may be another benefit to overlooking your lucky breaks, which is it makes easier to justify our pace in society. If I have a lot of wealth or power, I can just chalk it up to my own intelligence, effort and perseverance. It makes in easier to accept inequality.

In one expirement, participants were put in groups of three in small  rooms to discuss a complex moral problem, and one person in each group was randomly designated to the team leader. Half an hour later, the experimeter came by with the four cookies for each team. So who got the extra cookie? in each case, it went to the team leader. Even though they had no special aptitude, didn't had extra responsibility and they'd gotten their position through chance alone.

Once you had achieved a certain status, it seems natural to feel like you deserve it and all the other good things that come your way.

In another experiment, participants were asked to think of a good thing that happened to them recently, and then one group was asked to list their own personal qualities or actions that made that good thing happen. And another group was asked to list external factor to list factors beyond their control and that led to the event. On completing this task, participants were told they would be paid a dollar, but at the end they were offered the option to donate some or all of the money to a charity. Results showed those who listed their own personal attitude contributes 25% less.

Thinking about of what all these means for people in our society, specifically for people in positions of power like business leaders and politicians. And undoubtedly most of them are talented and hard-working, but they have also been luckier than most of us, they don't realize how lucky they are.

And this creates a distorted view of reality. They are kind of living in a form of survivor bias: all these leaders have worked hard and ultimately succeeded, so to them world appears fair. In their experience, it rewards hard work. But what they dont have is the experience of all people who have worked hard and failed. So what are they to make of people less successful than themselves? Well the natural conclusion is they must be less talented or less hard-working.

And this perspective makes them less inclined to be generious - to give back. And they are the ones who setthe rules for how society operates. And this is particularly unfortunate since one of the main ways many of us are lucky is in our country of residence.

But what is a country except for the things put there by the poople who came before? The roads and the schools, public transport, emergency services, clean air and water, everything like that. It seems a cruel trick of our phychology that successful people without any malice will credit their success largely to their own hard work and ingenuity and therefore contribute less to maintaining the very circumstances that made that success possible in the first place.

The good news is that acknowledge our fortunate circumstances not only bring us more in the line with reality, it also makes us more like-able. In a study where people had to read the transcript of a fictional 60-minutes interview with the bio-tech entrepreneur, experimenters tried changing just the last paragraph where the interviewee is talking about the reasons for the company's success. In one version, the entrepreneur personally takes credit for the success they've had, but in the other he says luck played an significant role.

Now people who read the luck version of the transcript judged the entreprenuer as kinder, and thought they'd be more likely to be close friends with him than those who read the other version of the transcript. 

Raising our awareness of fortunate events can also makes us happier because it allows us to feel gratutude. Now initially, I wanted to write this page just to say our circumstances and phychology conspire to make us obvlious to our own luck.

This leads successful people to view the world as fair,and those less successful than them as less talented or less hard-working. And this is before you factor in any discrimination or prejudice. But it also became apparent to me that I should talk about what to do if you want to be successful in such world,

And I think the best advice is paradoxical. First, you must believe that you are in complete control of your destiny, and your success comes down only to your own talent and hard work. But second, you've got to know that's not true for you or anyone else. So you have to remember: if you do achieve success that luck played a significant role and given your good fortune, you should do what you can to increase the luck of others.

