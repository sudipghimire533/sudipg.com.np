---
title: "Rust Workshop - iBriz academy 2022"
keywords: ["rust", "sudip"]
date: 2022-02-07T20:00:00+05:45
tags: ["rust", "workshop", "blockchain"]
categories: ["technical"]
description: "Workshop on rust, it's introduction use case and it's potential"
series: ["learning-rust"]
slug: "rust-workshop-2022-ibriz"
aliases: ["ibriz-rust-workshop"]
images: []
toc: true
comment: true
math: false


---

This was the reference to which I presented an hour long workshop within iBriz Academy. This contains broad introduction ( not too depth ) to introduce anyone who is familiar with programming in general. I also showcased my ytui-music project.
<!--more-->
![Rust crab looks happy](https://sudipg.com.np/images/featured-images/happy-rust-crab.png)

##  What will we discuss

* What are we using in our current project
  - Rust  
  - Substrate
  - Polkadot.js
  - Lots and lots of communication and helping each other
* Why Rust for blockchain
* Why rust even for anything else
* See a project (ytui-music) developed entirely on rust
* Have a high level understanding of how ytui-music work
* Navigate a bit of code here and there
* Future of rust and where else we can expect it to emerge
* Reaching me out



# Ytui-music

Listen to music from youtube in terminal with decent tui

## Why use ytui-music

* Never get tracked by youtube on your listening preference
* Downloading music made easiest
* Save machine resource for other useful task by avoiding browser
* Use on remote ssh or terminal-only session
* Completely keyboard driven usage ( feed the cats with mouse )
* Configurable ui and other preferences with easy json file
* Open sourced codebase to make it all yours

## Screenshots

====> See direct demo <=====

## Highlights from demo

1. Search according to types. eg: music, playlist, artist, all
2. See trending feed
3. Pagination with `p` and `n`
4. Responsive design
5. Configurable with easy json
   * Default for dark theme, can be made for light
6. Saved data in sqlite format
7. suffle/ repeat/ play pause
8. Volume control in master branch
9. configurable mpv => configurable youtube-dl
10. navigation
11. download

## Tools and Libraries used

1) Rust ( https://github.com/rust-lang/rust/ ) - to write ideas
2) Invidious (https://github.com/iv-org/invidious)
   * middleware to serve youtube data in easy api
3) Mpv (https://github.com/mpv-player/mpv)
   * Used for playback
   * Supports youtube link out of the box as regulat file input
   * Not-so-heavy and comes with configurable options
   * Provides well documented and easy C api
4) libmpv-rs ( https://github.com/ParadoxSpiral/libmpv-rs )
   * Rust binding for mpv
5) tui-rs ( https://github.com/fdehau/tui-rs )
   * Allows developing tui with rust
   * Supports multiple backends like termion, crossterm etc.
6) Corss-term ( https://github.com/crossterm-rs/crossterm )
   * Used internally by tui-rs ( for this project )
   * Support keyboard ( as well as mouse ) event handeling
7) Youtube-dl ( https://github.com/ytdl-org/youtube-dl )
   * Downloading of music from youtube server
   * Internally used by mpv for youtube-music playback
8) Sqlite (https://sqlite.org/src/)
   * Store favourates on local system
9) Neovim, Rust-analyzer, COQ_nvim, Typora, action-rs .... - for development proposes
9) Github ( https://github.com ) - For hosting suorce code

## Working Mechanism



## You might want to work on

Depending on what you have already been into

### UI

* Add pop ups and over-screen widgets
* Optional audio visualizer
* Configurable section size or even more configuriable layout
* Pre-configured colour scheme

### Web/ Network/ Scraping

* Add support for spotify or other services like deezer, apple music
* Deploy own invidious server
* Music recommendation based on listening activity ( youtube's recommended for you list )

### Media handling

* Allow custom playback
* Filters and mixing

### Database

* Custom playlist ( other than favorates )
* Most listened songs
* Import playlist from youtube favourates

### Others..

* Performance optimization
* Another rust binding for libmpv
* Docs
* And whatever you want to





## Why rust for ytui-music & anything else?

- Good to go concurrency

  * Cross platform abstracted concurrent programming

  * First class async support in addition
  * Lifetime to say goodbye to deadlock, global variable override and segfault
  * Mutex, ConditionVariable, Arc & Rc to make things even better
- Safe and well-designed library

  * Rs-tui - a library that makes developing tui fun and crossplatform
  * libmpv-rs - safe and abstract binding over mpv player
  * reqwest - easy and battery-included http library
  * serde - life saver to work with popular human-redable format
- Forces to write GOOD code

  * Never create copy when you can move
  * simplify similar logic with declerative macros ( not same as generic though )
  * make writing platoform-dependent code more easy with conditional macros
- And lot more...

## Build file in rust

![img](https://sudipg.com.np/images/ibriz-workshop-2022/meme-make.jpg)



## Great innovation with documentation & Testing

* Makes writing documentation fun, easy and encouraging

* Super easy to generate documentation

  `cargo doc`

* Super easy documentation navigating web-pages with heavy interlinking

  Click - click - click

  \* A quick navigation in docs.rs

* Support for documentation code snippet checking

  Keep code example in sync with changes in codebase

* Built-in test framework & benchmarking framework

  \* Writing test doesn't add complexity in logic

  \* Full freedom with macros like `#[cfg(test)]`

## Configurable formatting

* Cargo fmt

  ====> *Lets see a demo*

# Future of Rust

## Team behind Rust

- The Rust Foundation 

  \* Non profit organization with bigger vision and devoted community

  \* https://foundation.rust-lang.org/
  
  \* Backed by tech gaints including but not limited to Google, Mozilla, Linux Foundation, Meta Inc (Facebook), Microsoft, Huawei ....

## Blockchain Development

* Type stirctlesafe

* Macro to develop basic DSL ( Domain Specific Language)

  * Ink! ( https://github.com/paritytech/ink )

    \* Make heavy use of macro to enable writing smart contract within rust

* Aggressive check for possible error

*  hard to get undefined behaviour and segmentation fault once compiled

* Compiles to WASM

  * https://rustwasm.github.io/docs/book/

  * One of very few language to have support for wasm from it's beginning

  * Rich eco-system with several `#![no_std]`compatible libraries

    \* Parity-scale codec ( encoding/decoding library with wasm support )

* Powerful modular framework like substrate & solana

  \* https://substrate.dev

  \* https://docs.solana.com/developing/on-chain-programs/developing-rust

* Environment friendly blockchain

  Many steps been made like preferring POS instead of POW rust brings some energy with it

  - Proven to be very energy efficint

    ![](https://sudipg.com.np/images/ibriz-workshop-2022/energy-index.png)

    Reference: https://thenewstack.io/which-programming-languages-use-the-least-electricity/

    ![img](https://sudipg.com.np/images/ibriz-workshop-2022//energy-index2.jpg)

    <img src="https://sudipg.com.np/images/ibriz-workshop-2022/energy-index3.jpg" alt="img" />
  
    Reference: https://sites.google.com/view/energy-efficiency-languages/updated-functional-results-2020

## Embedded system & IOT

* Learning resources

  * https://docs.rust-embedded.org/discovery/
  * https://docs.rust-embedded.org/book/
  * https://docs.rust-embedded.org/embedonomicon/

* Powerful static analysis

  * Do not compile risky codes

* Interoperability

  * Integrate well with existing embedded ecosystem which is dominated by C & C++
  * Can write assembly directly

* Portability

  * Write once and run everywhere goal

* RTOS ( Real Time Operating System ) being developed in rust

  * Tockos https://www.tockos.org/
  * Drone-os https://www.drone-os.com/

* Several platform and new microprocessor are supporting rust out-of-box

  * Supports various platform at various tire level including

    \* aarch64

    \* arm

    \* intel

    \* armebv7r

    \* asmjs

    \* thumbv6m

    Refernce: https://doc.rust-lang.org/nightly/rustc/platform-support.html

  * Microcontrollers kits you can program with rust

    \* Raspberry-pi https://www.raspberrypi.org/

    \* Aurdino https://www.arduino.cc/

    \* Micro bit https://tech.microbit.org/hardware/

    ![img](https://sudipg.com.np/images/ibriz-workshop-2022/microbit-v2.jpg)

    \* STM kits eg: STM32F303VC https://www.st.com/en/evaluation-tools/stm32f3discovery.html

    ![img](https://sudipg.com.np/images/ibriz-workshop-2022/f3-mcu-kit.jpg)

    \* Everything else LLVM already laid ground for until this much decade

    

## Kernel Development

* No more undefined behaviour
* No more dependency-hell
* No more overflow security holes

* Brand new Os written entirely in rust

  https://www.redox-os.org/

* Google Fuchsia os actively using rust

  https://fuchsia.dev/fuchsia-src/development/languages/rust

* Made an entry to linux kernel

  ![img](https://sudipg.com.np/images/ibriz-workshop-2022/tux-loves-rust.png)

  ​								
  
  \* https://www.zdnet.com/article/rust-takes-a-major-step-forward-as-linuxs-second-official-language/
  
  \* https://github.com/Rust-for-Linux/linux
  
  

## Cloud Computing, IOT and embeded system

* Loved by amazon AWS team

  \* https://aws.amazon.com/blogs/opensource/innovating-with-rust/

* Loved by cloudfare

  \* Native rust support on cloudfare workers

  \* https://blog.cloudflare.com/workers-rust-sdk/

* Easy on pocket (Runs with minimal resources)
* Easy cross-compiling in development process
* Supports lots of architecture

## Web Development

* Soon-to-be mature asynchronous ecosystem with tokio

* Web Framework

  - Actix web: https://actix.rs/

    Benchmark proves it to be fastest web framework

    Support completly async programming

  - Rocket-rs: https://rocket.rs/

    Easiest to start with

    Full async support

    Great documentation

  ## Personal utilities and fun projects

* Ever-growing ecosystem for libraries and framwork
* Community to turn to when needed
* Fun to write
* Easy to maintain
* Easy to reason about so easy for contributers

## The community inside rust

===> We have already talked about this <===

## Respects existing era

===> We have already talked about this <===



## While developing along the way

* Cargo expand ( `cargo install cargo-expand` )

  \* An example

* Godbolt ( https://godbolt.org )

  \* An demonstration

* Rust-analyser ( https://github.com/rust-analyzer/rust-analyzer )

  \* Showcase

* Rust playground ( https://play.rust-lang.org )

* Alternative Linker to improve build time

  \* GCC  <=== super stable & default

  \* Mold ( https://github.com/rui314/mold ) <=== fastest but not yet ready for production

  \* lld ( clang bundle ) <== balance between stable and speed

  \* Learn to configure

  ![Link speed comparison](https://sudipg.com.np/images/ibriz-workshop-2022/comparison-linker.png)

# Might not be all roses

##  What is Rust not for (yet)

- AI/Ml
- Mobile development
- Enterprise reasy gui development ( may be a good go within next week! who knows? )
  - Although tools like gtk-rs, qt-rs, sixtyfps are standing out to be leading tools
  - Tauri-rs already being used in replacement of electron

### Lots of compile time nap

![img](https://sudipg.com.np/images/ibriz-workshop-2022/compile-time-nap.jpeg)

​								Image credit: cnbc.com

* Slow to compile ( you can travel galaxy when you leave your project to compile for release )

  \* https://blog.rust-lang.org/2020/12/16/rust-survey-2020.html#compile-times

* Have to do a lot of checks and macro expansion

* Aggressive optimization in release mode

* Pack lots of helpful debug information and detailed tracebacks
* LLVM is not so compile time friendly (have several technical debts and code from every generation)

* Many tactics and tools to make it a breeze
  - Do not always build in release mode
  - Make use of `cargo check` and `cargo watch`
  - Test in test cases instead of testing it the-human-way by seeing logs
  - Use an lsp for instant feedback
  - Think more before you run

### And ...

* Not so old enough to have a planet-heavy ecosystem as compared to C or Python
* Do not have that much vast majority of developer. ( Pretty impressive how everybody knows javascript )
* Do not runs on 3 millions of devices like Java ( instead Rust is about to run on billions )
* Increase your frustration as compiler always throws error of every possible error (which is actually a benefit in long run)
* Not so quick to write prototyes


# Reaching me out

 ## About me

```rust
let me = Human {
    name: "Sudip Ghimire",
    location: "Pokhara",
    academic_qualification: "+2 passout",
    Professional_experience: [
        						"Web developer intern - Codesastra (2019)",
        						"Rust developer - iBriz.ai (2021)"
    						],
    email: ["sudipghimire533@gmail.com", "sudipg@ibriz.ai"],
    github: "github.com/sudipghimire533",
    website: "me.sudipg.com.np",
    blogs: "sudipg.com.np"
}
```



