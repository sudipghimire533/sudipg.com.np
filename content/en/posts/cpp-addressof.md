---
title: "C++ Idiom : Address-of | Get address of object of overloaded ‘&’ operator"
keywords: ["cpp", "c++", "oop", "lean object oriented programming", "address-of"]
date: 2020-10-11T12:59:25+05:45
tags: ["cpp", "cpp-idiom"]
categories: ["technical"]
description: "Function with default argument in rust. Default argument allows us to call function omitting some argumnt."
series: ["learning-cpp"]
slug: "cpp-address-of-idiom"
aliases: []
images: []
toc: true
comment: true
math: false

---

## Intent

To get the address of object of a class that have overloaded ‘&’ operator that do not return real address of object

## Side Note

The idiom presented in this article is no longer needed to be hand written on the compiler that supports at least c++11 standard. More tested `std::addressof()` in header `<memory>` should be used instead. This article just give the idea and the reason to use such function.

## Motivation

C++ allows address-of operator(&) to be overloaded inside the user defined type. That overloaded operator need not strictly return the actual address of the object. Some class misuse this power and create a confusion especially when used in library code. However such action should be done very carefully as it break the general structure of code.
For the quick example look at the code below in picture. In the following code we had created a class Demo and overloaded ‘&’ operator to return address of data member i.e {int a}. The operation inside main() for both class (‘Fine’ and ‘Demo’) is same but assignment of address of d2 to d1 gives the very very unexpected error. If the similar code is inside the library where we cannot modify source it is very unpredictable to know the actual intent of doing so.

![img](https://sudipart.files.wordpress.com/2020/09/image-12.png?w=1024)

However if we are supposed to solve the error then what we need is to write address-of idiom.

## Solution and sample code

The main idea of this idiom is to get the actual address of provided object by making the given argument go through series of cast

![img](https://sudipart.files.wordpress.com/2020/09/image-13.png?w=672)

You can get the source code of the example used in this article at https://github.com/sudipghimire533/learning/blob/master/idioms/addressof.cpp

## Reference

https://en.cppreference.com/w/cpp/memory/addressof